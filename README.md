Tooltip on hovering a Text Field, Drupal 8 module
=================================================

Installation with Composer
==========================

on <drupal8-root> directory, in the command line run in order the following 3 commands:

    mkdir -p modules/custom
    
    git -C modules/custom clone https://bitbucket.org/panchocadena/tooltip
  

    composer require drupal/libraries
  

    git -C vendor clone https://github.com/qTip2/qTip2.git
  


  enable module

  on text field display settings set configuration.
