/**
 * @file
 * Loads the qTip2 library.
 */
var $j = jQuery.noConflict();

$j(document).ready(function(){
	$j('.text_field_tooltip_qTip2_formatter').qtip({
		content: {
	      text: 'Colibricode.com: web, mobil or apps'
		  },
	    events: {
        render: function(event, api) {
          // Grab the overlay element
          var elem = api.elements.overlay;
    	  }
  		}
  	});
});